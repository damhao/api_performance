package types

var (
	RequestChannel  = "request"
	ResponseChannel = "response"
)

type Post struct {
	ID          int    `json:"id"`
	URL         string `json:"url"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Thumbnail   string `json:"thumbnail"`
	Detail      string `json:"detail"`
	Topic       string `json:"topic"`
}

func (PostTable) TableName() string {
	return "data_vn"
}

type PostTable struct {
	ID          int `gorm:"primaryKey"`
	URL         string
	Title       string
	Description string
	Thumbnail   string
	Detail      string
	Topic       string
}

type PostDetail struct {
	Type        string `json:"type"`
	Value       string `json:"value"`
	Description string `json:"description,omitempty"`
}

type DBConfig struct {
	ServerName string
	User       string
	Password   string
	DB         string
}
type Error struct {
	Error   string
	Message string
}

type RedisRequest struct {
	Name   string
	Params map[string]string
	Id     int
}

type RedisResponse struct {
	Data string
	Id   int
}

func NewRedisResponse(data string, id int) RedisResponse {
	return RedisResponse{
		Data: data,
		Id:   id,
	}
}
