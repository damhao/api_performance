package client

import (
	"github.com/go-redis/redis/v8"
)

var Client *redis.Client

func InitRedisClient() {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	Client = rdb

	for {

	}

	// return rdb
}
