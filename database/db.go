package database

import (
	cfg "API/configs"
	"fmt"
	"log"

	"API/types"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func InitConnection() *gorm.DB {

	c := cfg.GetConfig()
	servername := c.GetString("ServerName")
	user := c.GetString("User")
	password := c.GetString("Password")
	db := c.GetString("DB")

	config :=
		types.DBConfig{
			ServerName: servername,
			User:       user,
			Password:   password,
			DB:         db,
		}

	connectionString := GetConnectionString(config)
	connector := InitDB(connectionString)

	connector.AutoMigrate(
		//&types.Post{},
		&types.PostTable{},
		//&types.PostDetail{},
	)
	log.Println(string("\033[33m"), "Tables migrated")

	return connector

}

func InitDB(connectionString string) *gorm.DB {
	//fmt.Println("=====")
	var err error
	connector, err := gorm.Open(mysql.Open(connectionString), &gorm.Config{
		// SkipDefaultTransaction: true,
		PrepareStmt: true,
	})
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Connection was successful!!")
	return connector

}

var GetConnectionString = func(config types.DBConfig) string {
	connectionString := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True&loc=Local",
		config.User, config.Password, config.ServerName, config.DB)
	return connectionString
}
