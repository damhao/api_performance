package utils

import (
	"os"
)

func ExportFile(data, filePath string) error {

	f, err := os.OpenFile(filePath, os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		return err
	}
	defer f.Close()

	if _, err = f.WriteString(data); err != nil {
		return err
	}
	return nil
}
