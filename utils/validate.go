package utils

import (
	"fmt"
	"strconv"
)

func ValidateInputIsNumber(id string) (bool, error) {
	index, err := strconv.Atoi(id)

	if err != nil {
		fmt.Println("enter a number ")
		return false, err
	}

	if index < 0 {
		err = fmt.Errorf("Invalid ID please re-enter: ")
		fmt.Println(err)
		return false, err
	}
	return true, nil

}

func ValidateInputIsString(str string) (bool, error) {
	if len(str) == 0 {
		err := fmt.Errorf("String must not be blank")
		fmt.Println(err)
		return false, err
	}
	return true, nil
}
