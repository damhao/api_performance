package utils

import (
	"API/types"
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
)

func InitRedisRequest(ctx context.Context, client *redis.Client, param map[string]string, requestName string, index int) error {

	var request = types.RedisRequest{
		Name:   requestName,
		Params: param,
		Id:     index,
	}

	requestJSON, err := json.Marshal(&request)
	if err != nil {
		return fmt.Errorf("can't marshal request : %v", err)
	}

	client.Publish(ctx, types.RequestChannel, requestJSON)

	return nil
}

func GetRedisResponse(ctx context.Context, client *redis.Client, index int) (string, error) {
	sub := client.Subscribe(ctx, types.ResponseChannel)

	var result types.RedisResponse

	for {
		time.Sleep(time.Second)
		message, err := sub.ReceiveMessage(ctx)
		if err != nil {
			return "", fmt.Errorf("error when receiving message : %v", err)
		}

		err = json.Unmarshal([]byte(message.Payload), &result)
		if err != nil {
			return "", fmt.Errorf("error when unmarshal message : %v", err)
		}

		if result.Id == index {
			return result.Data, nil
		} else {
			continue
		}

	}

}
