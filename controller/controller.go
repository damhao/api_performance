package controller

import (
	"API/models"
	"API/response"
	"API/utils"
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"time"

	"API/types"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-redis/redis/v8"
)

type PostService struct {
	// api.PostInterface
	FilePath    string
	PostModel   *models.PostModel
	RedisClient *redis.Client
}

func InitPostService(postModel *models.PostModel, redisClient *redis.Client, filePath string) *PostService {
	return &PostService{
		PostModel:   postModel,
		FilePath:    filePath,
		RedisClient: redisClient,
	}
}

func (p *PostService) GetPostByID(w http.ResponseWriter, h *http.Request) {
	now := time.Now()

	paramStr := h.URL.Query().Get("id")
	valid, err := utils.ValidateInputIsNumber(paramStr)
	if !valid {
		response.ResponseError(w, 500, err)
		return
	}

	id, _ := strconv.Atoi(paramStr)

	ctx := context.Background()
	var param = make(map[string]string)
	param["id"] = fmt.Sprint(id)
	requestName := "getPostByID"

	index, err := p.RedisClient.Get(ctx, "id").Int()
	if err != nil {
		response.ResponseError(w, 500, fmt.Errorf("can't Get id : %v", err))
		return
	}

	err = p.RedisClient.Incr(ctx, "id").Err()
	if err != nil {
		response.ResponseError(w, 500, fmt.Errorf("can't increase id from redis : %v", err))
		return
	}

	err = utils.InitRedisRequest(ctx, p.RedisClient, param, requestName, index)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	result, err := utils.GetRedisResponse(ctx, p.RedisClient, index)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	var post types.PostTable

	err = json.Unmarshal([]byte(result), &post)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : GetPostByID\nINPUT : id = %d\nOUTPUT : %+v\nCREATED_TIME : %v\n\n", "GET", id, post, now), p.FilePath)
	if err != nil {
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponsePost(w, 200, post)

}
func (p *PostService) GetAllPosts(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	pageStr := h.URL.Query().Get("page")
	valid, err := utils.ValidateInputIsNumber(pageStr)
	if !valid {
		response.ResponseError(w, 500, err)
		return
	}

	page, _ := strconv.Atoi(pageStr)

	limitStr := h.URL.Query().Get("limit")
	valid, err = utils.ValidateInputIsNumber(limitStr)
	if !valid {
		response.ResponseError(w, 500, err)
		return
	}

	limit, _ := strconv.Atoi(limitStr)

	ctx := context.Background()

	var param = make(map[string]string)
	param["page"] = fmt.Sprint(page)
	param["limit"] = fmt.Sprint(limit)

	index, err := p.RedisClient.Get(ctx, "id").Int()
	if err != nil {
		response.ResponseError(w, 500, fmt.Errorf("can't get id from redis : %v", err))
		return
	}

	err = p.RedisClient.Incr(ctx, "id").Err()
	if err != nil {
		response.ResponseError(w, 500, fmt.Errorf("can't increase id from redis : %v", err))
		return
	}

	err = utils.InitRedisRequest(ctx, p.RedisClient, param, "getAllPosts", index)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	result, err := utils.GetRedisResponse(ctx, p.RedisClient, index)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	var posts []types.PostTable
	err = json.Unmarshal([]byte(result), &posts)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : GetAllPosts\nINPUT : page = %d, limit = %d\nOUTPUT : %+v\nCREATED_TIME : %v\n\n", "GET", page, limit, posts, now), p.FilePath)
	if err != nil {
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponseWithJson(w, http.StatusOK, posts)

}
func (p *PostService) DeletePostByID(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	paramStr := h.URL.Query().Get("id")

	valid, err := utils.ValidateInputIsNumber(paramStr)
	if !valid {
		response.ResponseError(w, 500, err)
		return
	}
	id, _ := strconv.Atoi(paramStr)

	ctx := context.Background()
	var param = make(map[string]string)

	param["id"] = fmt.Sprint(id)
	requestName := "deletePostByID"

	index, err := p.RedisClient.Get(ctx, "id").Int()
	if err != nil {
		response.ResponseError(w, 500, fmt.Errorf("can't get id from redis : %v", err))
		return
	}

	err = p.RedisClient.Incr(ctx, "id").Err()
	if err != nil {
		response.ResponseError(w, 500, fmt.Errorf("can't increase id from redis : %v", err))
		return
	}

	err = utils.InitRedisRequest(ctx, p.RedisClient, param, requestName, index)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	result, err := utils.GetRedisResponse(ctx, p.RedisClient, index)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : DeletePostByID\nINPUT : id = %d\nOUTPUT :\nCREATED_TIME : %v\n\n", "DELETE", id, now), p.FilePath)
	if err != nil {
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}
	response.ResponseWithJson(w, http.StatusOK, result)
}

func (p *PostService) CreatePost(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	body, err := ioutil.ReadAll(h.Body)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	var post types.PostTable
	err = json.Unmarshal(body, &post)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	ctx := context.Background()

	index, err := p.RedisClient.Get(ctx, "id").Int()
	if err != nil {
		response.ResponseError(w, 500, fmt.Errorf("can't get id from redis : %v", err))
		return
	}

	err = p.RedisClient.Incr(ctx, "id").Err()
	if err != nil {
		response.ResponseError(w, 500, fmt.Errorf("can't increase id from redis : %v", err))
		return
	}

	err = p.RedisClient.Incr(ctx, "id").Err()
	if err != nil {
		response.ResponseError(w, 500, fmt.Errorf("can't increase id from redis : %v", err))
		return
	}

	var param = make(map[string]string)

	param["post"] = string(body)

	err = utils.InitRedisRequest(ctx, p.RedisClient, param, "createPost", index)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	result, err := utils.GetRedisResponse(ctx, p.RedisClient, index)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : CreatePost\nINPUT : post = %+v\nOUTPUT :\nCREATED_TIME : %v\n\n", "POST", post, now), p.FilePath)
	if err != nil {
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponseWithJson(w, http.StatusOK, result)
}
func (p *PostService) UpdatePost(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	body, err := ioutil.ReadAll(h.Body)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	var post types.PostTable
	err = json.Unmarshal(body, &post)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	ctx := context.Background()

	index, err := p.RedisClient.Get(ctx, "id").Int()
	if err != nil {
		response.ResponseError(w, 500, fmt.Errorf("can't get id from redis : %v", err))
		return
	}

	err = p.RedisClient.Incr(ctx, "id").Err()
	if err != nil {
		response.ResponseError(w, 500, fmt.Errorf("can't increase id from redis : %v", err))
		return
	}

	var param = make(map[string]string)

	param["post"] = string(body)

	err = utils.InitRedisRequest(ctx, p.RedisClient, param, "updatePost", index)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	result, err := utils.GetRedisResponse(ctx, p.RedisClient, index)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : UpdatePost\nINPUT : post = %+v\nOUTPUT :\nCREATED_TIME : %v\n\n", "PUT", post, now), p.FilePath)
	if err != nil {
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponseWithJson(w, http.StatusOK, result)

}
func (p *PostService) GetPostByTitle(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	title := h.URL.Query().Get("title")

	valid, err := utils.ValidateInputIsString(title)
	if !valid {
		response.ResponseError(w, 500, err)
		return
	}

	posts, err := p.PostModel.GetPostByTitle(title)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : GetPostByTitle\nINPUT : title = %s\nOUTPUT : %+v\nCREATED_TIME : %v\n\n", "GET", title, posts, now), p.FilePath)
	if err != nil {
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponseWithJson(w, 200, posts)

}

func (p *PostService) GetPostByTopic(w http.ResponseWriter, h *http.Request) {
	now := time.Now()
	topic := h.URL.Query().Get("topic")

	valid, err := utils.ValidateInputIsString(topic)
	if !valid {
		response.ResponseError(w, 500, err)
		return
	}

	posts, err := p.PostModel.GetPostByTopic(topic)
	if err != nil {
		response.ResponseError(w, 500, err)
		return
	}

	err = utils.ExportFile(fmt.Sprintf("METHOD : %s\nACTION : GetPostByTopic\nINPUT : topic = %s\nOUTPUT : %+v\nCREATED_TIME : %v\n\n", "GET", topic, posts, now), p.FilePath)
	if err != nil {
		response.ResponseError(w, 500, errors.New("error when writing to file "))
		return
	}

	response.ResponseWithJson(w, 200, posts)

}
