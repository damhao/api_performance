package handler

import (
	"API/controller"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func Handle(postService *controller.PostService) {
	router := mux.NewRouter()
	router.HandleFunc("/create", postService.CreatePost).Methods("POST")
	router.HandleFunc("/posts", postService.GetAllPosts).Methods("GET")
	router.HandleFunc("/post", postService.GetPostByID).Methods("GET")
	router.HandleFunc("/delete", postService.DeletePostByID).Methods("DELETE")
	router.HandleFunc("/update", postService.UpdatePost).Methods("PUT")
	router.HandleFunc("/getByTitle", postService.GetPostByTitle).Methods("GET")
	router.HandleFunc("/getByTopic", postService.GetPostByTopic).Methods("GET")

	http.Handle("/", router)
	log.Println(string("\033[32m"), "API is running!")
	log.Fatalln(http.ListenAndServe("localhost:8080", router))

}
