package response

import (
	"API/types"
	"encoding/json"
	"fmt"
	"net/http"
)

func JSON(w http.ResponseWriter, statusCode int, data interface{}) {
	w.WriteHeader(statusCode)
	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		fmt.Fprintf(w, "%s", err.Error())
	}
}

func ERROR(w http.ResponseWriter, statusCode int, err error) {
	if err != nil {
		JSON(w, statusCode, struct {
			Error string `json:"error"`
		}{
			err.Error(),
		})
		return
	}
	JSON(w, http.StatusBadRequest, nil)
}

func ResponseWithJson(writer http.ResponseWriter, status int, objects ...interface{}) {
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(status)
	json.NewEncoder(writer).Encode(objects)
}

func ResponseError(writer http.ResponseWriter, status int, err error) {
	fmt.Println("Error", err)
	var errResponse = types.Error{
		Error:   err.Error(),
		Message: "LOI NAY",
	}
	//fmt.Println(err)
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(status)
	json.NewEncoder(writer).Encode(errResponse)

}

func ResponsePost(writer http.ResponseWriter, status int, object interface{}) {
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(status)
	json.NewEncoder(writer).Encode(object)
}
