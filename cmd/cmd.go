package cmd

import (
	"API/controller"
	"API/database"
	handler "API/handlers"
	"API/models"
	"API/service"
	"context"
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "root",
	Short: "rootCmd",
	Run: func(cmd *cobra.Command, args []string) {
		// Do Stuff Here
	},
}
var apiCmd = &cobra.Command{
	Use:   "api",
	Short: "api",

	Run: func(cmd *cobra.Command, args []string) {
		// Do Stuff Here
		db := database.InitConnection()
		postModel := models.InitPostModel(db)
		filePath := "D:/API_Ex3/log/api.log"
		redisClient := service.InitRedisClient()
		r := redisClient.Set(context.Background(), "id", 1, 0)
		if r.Err() != nil {
			log.Fatal(r.Err())
		}
		postService := controller.InitPostService(postModel, redisClient, filePath)
		handler.Handle(postService)
	},
}

var TaskQueueCmd = &cobra.Command{
	Use:   "taskqueue",
	Short: "task queue",

	Run: func(cmd *cobra.Command, args []string) {
		// Do Stuff Here
		service.TaskQueue()
	},
}

func init() {
	rootCmd.AddCommand(apiCmd)
	rootCmd.AddCommand(TaskQueueCmd)
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
