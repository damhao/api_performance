package service

import (
	"API/database"
	"API/models"
	"API/types"
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
)

func TaskQueue() {
	fmt.Println("Task queue is running !")
	ctx := context.Background()

	rdb := InitRedisClient()

	db := database.InitConnection()

	p := models.InitPostModel(db)

	pubsub := rdb.Subscribe(ctx, types.RequestChannel)

	for {
		message, err := pubsub.ReceiveMessage(ctx)
		if err != nil {
			fmt.Printf("Error when receiving message : %v\n", err)
		}

		fmt.Printf("received %+v\n", message.Payload)

		var request types.RedisRequest
		err = json.Unmarshal([]byte(message.Payload), &request)
		if err != nil {
			fmt.Printf("Can't unmarshal message : %v\n", err)
		}

		switch request.Name {

		case "getAllPosts":
			page, err := strconv.Atoi(request.Params["page"])
			if err != nil {
				fmt.Println(err)
			}
			limit, err := strconv.Atoi(request.Params["limit"])
			if err != nil {
				fmt.Println(err)
			}
			posts, err := p.GetAllPosts(page, limit)
			if err != nil {
				fmt.Println(err)
			}

			postJSON, err := json.Marshal(&posts)
			if err != nil {
				fmt.Println(err)
				rdb.Publish(ctx, types.ResponseChannel, err.Error())
			}

			response := types.NewRedisResponse(string(postJSON), request.Id)

			RedisSubResponse(response, rdb, ctx)

		case "getPostByID":
			id, err := strconv.Atoi(request.Params["id"])
			if err != nil {
				fmt.Println(err)
			}
			post, err := p.GetPostByID(id)
			if err != nil {
				fmt.Println(err)
			}
			postJSON, err := json.Marshal(&post)
			if err != nil {
				fmt.Println(err)
				rdb.Publish(ctx, types.ResponseChannel, err.Error())
			}

			response := types.NewRedisResponse(string(postJSON), request.Id)

			RedisSubResponse(response, rdb, ctx)

		case "createPost":
			postJSON := request.Params["post"]

			var post types.PostTable

			err := json.Unmarshal([]byte(postJSON), &post)
			if err != nil {
				fmt.Println(err)
			}

			var response types.RedisResponse
			err = p.CreatePost(post)
			if err != nil {
				fmt.Println(err)
				response = types.NewRedisResponse("Cant create post", request.Id)
			} else {
				response = types.NewRedisResponse("Created successfully", request.Id)
			}

			RedisSubResponse(response, rdb, ctx)

		case "deletePostByID":
			id, err := strconv.Atoi(request.Params["id"])
			if err != nil {
				fmt.Println(err)
			}
			var response types.RedisResponse

			err = p.DeletePostByID(id)
			if err != nil {
				fmt.Println(err)
				response = types.NewRedisResponse("failed to delete the article", request.Id)
			} else {
				response = types.NewRedisResponse("Delete successfully", request.Id)
			}

			RedisSubResponse(response, rdb, ctx)

		case "updatePost":
			postJSON := request.Params["post"]

			var post types.PostTable

			err := json.Unmarshal([]byte(postJSON), &post)
			if err != nil {
				fmt.Println(err)
			}

			var response types.RedisResponse
			err = p.UpdatePost(post)
			if err != nil {
				fmt.Println(err)
				response = types.NewRedisResponse("Cant update post", request.Id)
			} else {
				response = types.NewRedisResponse("Updated successfully", request.Id)
			}

			RedisSubResponse(response, rdb, ctx)

		}

		time.Sleep(time.Millisecond * 100)
	}

}

func InitRedisClient() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
}

func RedisSubResponse(response types.RedisResponse, rdb *redis.Client, ctx context.Context) {
	responseJSON, err := json.Marshal(&response)
	if err != nil {
		fmt.Println(err)
		rdb.Publish(ctx, types.ResponseChannel, err.Error())
	}

	pub := rdb.Publish(ctx, types.ResponseChannel, string(responseJSON))
	if pub.Err() != nil {
		fmt.Println(err)
		rdb.Publish(ctx, types.ResponseChannel, fmt.Errorf("error when sending response"))
	}
	fmt.Println(response)
}
